import request from '@/utils/request'

// 查询知识产权列表
export function listIntellectualProperty(query) {
  return request({
    url: '/intellectualProperty/intellectualProperty/list',
    method: 'get',
    params: query
  })
}

// 查询知识产权详细
export function getIntellectualProperty(id) {
  return request({
    url: '/intellectualProperty/intellectualProperty/' + id,
    method: 'get'
  })
}

// 新增知识产权
export function addIntellectualProperty(data) {
  return request({
    url: '/intellectualProperty/intellectualProperty',
    method: 'post',
    data: data
  })
}

// 修改知识产权
export function updateIntellectualProperty(data) {
  return request({
    url: '/intellectualProperty/intellectualProperty',
    method: 'put',
    data: data
  })
}

// 删除知识产权
export function delIntellectualProperty(id) {
  return request({
    url: '/intellectualProperty/intellectualProperty/' + id,
    method: 'delete'
  })
}
