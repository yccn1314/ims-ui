import request from '@/utils/request'

// 查询成果采纳列表
export function listAdoptedOutcomes(query) {
  return request({
    url: '/adoptedOutcomes/adoptedOutcomes/list',
    method: 'get',
    params: query
  })
}

// 查询成果采纳详细
export function getAdoptedOutcomes(id) {
  return request({
    url: '/adoptedOutcomes/adoptedOutcomes/' + id,
    method: 'get'
  })
}

// 新增成果采纳
export function addAdoptedOutcomes(data) {
  return request({
    url: '/adoptedOutcomes/adoptedOutcomes',
    method: 'post',
    data: data
  })
}

// 修改成果采纳
export function updateAdoptedOutcomes(data) {
  return request({
    url: '/adoptedOutcomes/adoptedOutcomes',
    method: 'put',
    data: data
  })
}

// 删除成果采纳
export function delAdoptedOutcomes(id) {
  return request({
    url: '/adoptedOutcomes/adoptedOutcomes/' + id,
    method: 'delete'
  })
}
