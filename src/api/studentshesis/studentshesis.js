import request from '@/utils/request'

// 查询学生论文管理列表
export function listStudentshesis(query) {
  return request({
    url: '/studentshesis/studentshesis/list',
    method: 'get',
    params: query
  })
}

// 查询学生论文管理详细
export function getStudentshesis(id) {
  return request({
    url: '/studentshesis/studentshesis/' + id,
    method: 'get'
  })
}

// 新增学生论文管理
export function addStudentshesis(data) {
  return request({
    url: '/studentshesis/studentshesis',
    method: 'post',
    data: data
  })
}

// 修改学生论文管理
export function updateStudentshesis(data) {
  return request({
    url: '/studentshesis/studentshesis',
    method: 'put',
    data: data
  })
}

// 删除学生论文管理
export function delStudentshesis(id) {
  return request({
    url: '/studentshesis/studentshesis/' + id,
    method: 'delete'
  })
}
