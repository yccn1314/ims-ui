import request from '@/utils/request'

// 查询竞赛管理列表
export function listCompetition(query) {
  return request({
    url: '/studentmanger/competition/list',
    method: 'get',
    params: query
  })
}

// 查询竞赛管理详细
export function getCompetition(id) {
  return request({
    url: '/studentmanger/competition/' + id,
    method: 'get'
  })
}

// 新增竞赛管理
export function addCompetition(data) {
  return request({
    url: '/studentmanger/competition',
    method: 'post',
    data: data
  })
}

// 修改竞赛管理
export function updateCompetition(data) {
  return request({
    url: '/studentmanger/competition',
    method: 'put',
    data: data
  })
}

// 删除竞赛管理
export function delCompetition(id) {
  return request({
    url: '/studentmanger/competition/' + id,
    method: 'delete'
  })
}
