import request from '@/utils/request'

// 查询学生实习项目经历列表
export function listStudentprojectexperience(query) {
  return request({
    url: '/studentprojectexperience/studentprojectexperience/list',
    method: 'get',
    params: query
  })
}

// 查询学生实习项目经历详细
export function getStudentprojectexperience(id) {
  return request({
    url: '/studentprojectexperience/studentprojectexperience/' + id,
    method: 'get'
  })
}

// 新增学生实习项目经历
export function addStudentprojectexperience(data) {
  return request({
    url: '/studentprojectexperience/studentprojectexperience',
    method: 'post',
    data: data
  })
}

// 修改学生实习项目经历
export function updateStudentprojectexperience(data) {
  return request({
    url: '/studentprojectexperience/studentprojectexperience',
    method: 'put',
    data: data
  })
}

// 删除学生实习项目经历
export function delStudentprojectexperience(id) {
  return request({
    url: '/studentprojectexperience/studentprojectexperience/' + id,
    method: 'delete'
  })
}
