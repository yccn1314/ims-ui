import request from '@/utils/request'

// 查询鉴定项目列表
export function listCheckProjects(query) {
  return request({
    url: '/checkProjects/checkProjects/list',
    method: 'get',
    params: query
  })
}

// 查询鉴定项目详细
export function getCheckProjects(id) {
  return request({
    url: '/checkProjects/checkProjects/' + id,
    method: 'get'
  })
}

// 新增鉴定项目
export function addCheckProjects(data) {
  return request({
    url: '/checkProjects/checkProjects',
    method: 'post',
    data: data
  })
}

// 修改鉴定项目
export function updateCheckProjects(data) {
  return request({
    url: '/checkProjects/checkProjects',
    method: 'put',
    data: data
  })
}

// 删除鉴定项目
export function delCheckProjects(id) {
  return request({
    url: '/checkProjects/checkProjects/' + id,
    method: 'delete'
  })
}
