import request from '@/utils/request'

// 查询奖学金信息管理列表
export function listScholarship(query) {
  return request({
    url: '/scholarship/scholarship/list',
    method: 'get',
    params: query
  })
}

// 查询奖学金信息管理详细
export function getScholarship(id) {
  return request({
    url: '/scholarship/scholarship/' + id,
    method: 'get'
  })
}

// 新增奖学金信息管理
export function addScholarship(data) {
  return request({
    url: '/scholarship/scholarship',
    method: 'post',
    data: data
  })
}

// 修改奖学金信息管理
export function updateScholarship(data) {
  return request({
    url: '/scholarship/scholarship',
    method: 'put',
    data: data
  })
}

// 删除奖学金信息管理
export function delScholarship(id) {
  return request({
    url: '/scholarship/scholarship/' + id,
    method: 'delete'
  })
}
