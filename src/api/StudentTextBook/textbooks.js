import request from '@/utils/request'

// 查询著作教材列表
export function listTextbooks(query) {
  return request({
    url: '/StudentTextBook/textbooks/list',
    method: 'get',
    params: query
  })
}

// 查询著作教材详细
export function getTextbooks(id) {
  return request({
    url: '/StudentTextBook/textbooks/' + id,
    method: 'get'
  })
}

// 新增著作教材
export function addTextbooks(data) {
  return request({
    url: '/StudentTextBook/textbooks',
    method: 'post',
    data: data
  })
}

// 修改著作教材
export function updateTextbooks(data) {
  return request({
    url: '/StudentTextBook/textbooks',
    method: 'put',
    data: data
  })
}

// 删除著作教材
export function delTextbooks(id) {
  return request({
    url: '/StudentTextBook/textbooks/' + id,
    method: 'delete'
  })
}
