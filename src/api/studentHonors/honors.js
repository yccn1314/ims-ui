import request from '@/utils/request'

// 查询学生荣誉列表
export function listHonors(query) {
  return request({
    url: '/studentHonors/honors/list',
    method: 'get',
    params: query
  })
}

// 查询学生荣誉详细
export function getHonors(id) {
  return request({
    url: '/studentHonors/honors/' + id,
    method: 'get'
  })
}

// 新增学生荣誉
export function addHonors(data) {
  return request({
    url: '/studentHonors/honors',
    method: 'post',
    data: data
  })
}

// 修改学生荣誉
export function updateHonors(data) {
  return request({
    url: '/studentHonors/honors',
    method: 'put',
    data: data
  })
}

// 删除学生荣誉
export function delHonors(id) {
  return request({
    url: '/studentHonors/honors/' + id,
    method: 'delete'
  })
}
