import request from '@/utils/request'

// 查询学生科研立项列表
export function listProjects(query) {
  return request({
    url: '/studentScienceProject/projects/list',
    method: 'get',
    params: query
  })
}

// 查询学生科研立项详细
export function getProjects(id) {
  return request({
    url: '/studentScienceProject/projects/' + id,
    method: 'get'
  })
}

// 新增学生科研立项
export function addProjects(data) {
  return request({
    url: '/studentScienceProject/projects',
    method: 'post',
    data: data
  })
}

// 修改学生科研立项
export function updateProjects(data) {
  return request({
    url: '/studentScienceProject/projects',
    method: 'put',
    data: data
  })
}

// 删除学生科研立项
export function delProjects(id) {
  return request({
    url: '/studentScienceProject/projects/' + id,
    method: 'delete'
  })
}
