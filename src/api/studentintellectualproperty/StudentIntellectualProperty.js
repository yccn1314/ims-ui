import request from '@/utils/request'

// 查询知识产权列表
export function listStudentIntellectualProperty(query) {
  return request({
    url: '/studentintellectualproperty/StudentIntellectualProperty/list',
    method: 'get',
    params: query
  })
}

// 查询知识产权详细
export function getStudentIntellectualProperty(id) {
  return request({
    url: '/studentintellectualproperty/StudentIntellectualProperty/' + id,
    method: 'get'
  })
}

// 新增知识产权
export function addStudentIntellectualProperty(data) {
  return request({
    url: '/studentintellectualproperty/StudentIntellectualProperty',
    method: 'post',
    data: data
  })
}

// 修改知识产权
export function updateStudentIntellectualProperty(data) {
  return request({
    url: '/studentintellectualproperty/StudentIntellectualProperty',
    method: 'put',
    data: data
  })
}

// 删除知识产权
export function delStudentIntellectualProperty(id) {
  return request({
    url: '/studentintellectualproperty/StudentIntellectualProperty/' + id,
    method: 'delete'
  })
}
