import request from '@/utils/request'

// 查询教师荣誉列表
export function listTeacherHonors(query) {
  return request({
    url: '/teacherHonors/teacherHonors/list',
    method: 'get',
    params: query
  })
}

// 查询教师荣誉详细
export function getTeacherHonors(id) {
  return request({
    url: '/teacherHonors/teacherHonors/' + id,
    method: 'get'
  })
}

// 新增教师荣誉
export function addTeacherHonors(data) {
  return request({
    url: '/teacherHonors/teacherHonors',
    method: 'post',
    data: data
  })
}

// 修改教师荣誉
export function updateTeacherHonors(data) {
  return request({
    url: '/teacherHonors/teacherHonors',
    method: 'put',
    data: data
  })
}

// 删除教师荣誉
export function delTeacherHonors(id) {
  return request({
    url: '/teacherHonors/teacherHonors/' + id,
    method: 'delete'
  })
}
