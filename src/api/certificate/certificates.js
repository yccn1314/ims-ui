import request from '@/utils/request'

// 查询志愿者证书管理列表
export function listCertificates(query) {
  return request({
    url: '/certificate/certificates/list',
    method: 'get',
    params: query
  })
}

// 查询志愿者证书管理详细
export function getCertificates(certificateId) {
  return request({
    url: '/certificate/certificates/' + certificateId,
    method: 'get'
  })
}

// 新增志愿者证书管理
export function addCertificates(data) {
  return request({
    url: '/certificate/certificates',
    method: 'post',
    data: data
  })
}

// 修改志愿者证书管理
export function updateCertificates(data) {
  return request({
    url: '/certificate/certificates',
    method: 'put',
    data: data
  })
}

// 删除志愿者证书管理
export function delCertificates(certificateId) {
  return request({
    url: '/certificate/certificates/' + certificateId,
    method: 'delete'
  })
}
